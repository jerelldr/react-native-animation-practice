import React from "react";
import { Constants } from "expo";
import { StyleSheet, Text, ScrollView, Dimensions } from "react-native";
import { Button, Card } from "react-native-elements";
import Deck from "./Deck";
import VerticalCard from "./VerticalCard";

const DEVICE_HEIGHT = Dimensions.get("window").height;

const DATA = [
  {
    id: 1,
    text: "Card #1",
    uri: "http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg"
  },
  {
    id: 2,
    text: "Card #2",
    uri: "http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg"
  },
  {
    id: 3,
    text: "Card #3",
    uri: "http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg"
  },
  {
    id: 4,
    text: "Card #4",
    uri: "http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg"
  },
  {
    id: 5,
    text: "Card #5",
    uri: "http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg"
  },
  {
    id: 6,
    text: "Card #6",
    uri: "http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg"
  },
  {
    id: 7,
    text: "Card #7",
    uri: "http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg"
  },
  {
    id: 8,
    text: "Card #8",
    uri: "http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg"
  }
];

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [
        {
          title: "Peace on earth declared",
          comment: "Piece of cake"
        },
        {
          title: "Cats superior to dogs, find scientists",
          comment: "*Goes back to sleep* — cats everywhere"
        },
        {
          title: "Pineapple on pizza ruled a flavour hazard",
          comment: "But what about beetroot on burgers?"
        }
      ]
    };
  }
  renderCard = item => {
    return (
      <Card
        key={item.id}
        title={item.text}
        image={{ uri: item.uri }}
        containerStyle={{ borderRadius: 25 }}
      >
        <Text style={{ marginBottom: 10 }}>
          I can customize the card further
        </Text>
        <Button
          icon={{ name: "code" }}
          backgroundColor="#03A9F4"
          title="View Now!"
        />
      </Card>
    );
  };

  renderNoMoreCards() {
    return (
      <Card title="All Done!">
        <Text style={{ marginBottom: 10 }}>There's no more content here!</Text>
        <Button backgroundColor="#03A9F4" title="Get more!" />
      </Card>
    );
  }
  render() {
    return (
      <ScrollView contentInsetAdjustmentBehavior="scrollableAxes">
        {this.state.cards.map((card, i) => {
          return (
            <VerticalCard
              {...card}
              height={DEVICE_HEIGHT / 3.75}
              index={i}
              key={i}
            />
          );
        })}
      </ScrollView>
    );
  }
  // render() {
  //   return (
  //     <View style={styles.container}>
  //       <View style={styles.statusBar}>
  //         <Deck
  //           data={DATA}
  //           renderNoMoreCards={this.renderNoMoreCards}
  //           renderCard={this.renderCard}
  //         />
  //       </View>
  //     </View>
  //   );
  // }
}

const styles = StyleSheet.create({
  statusBar: {
    backgroundColor: "rgb(44, 44, 44)",
    paddingTop: Constants.statusBarHeight
  },
  container: {
    flex: 1
  }
});
